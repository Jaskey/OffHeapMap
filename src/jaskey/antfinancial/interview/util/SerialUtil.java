package jaskey.antfinancial.interview.util;

import jaskey.antfinancial.interview.testcase.TestObject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;

/**
 * Created by jaskeylin on 2017/3/15.
 */
public class SerialUtil {
    public static byte[] serialize(Object object) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(object);
            out.flush();
            byte[] bytesArray = bos.toByteArray();
            return bytesArray;
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            Object o = in.readObject();
            return o;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TestObject origin = new TestObject("Test1");
        byte[]  bytes = SerialUtil.serialize(origin);
        TestObject o = (TestObject)SerialUtil.deserialize(bytes);
        System.out.println("序列化再反序列化后："+o);
        System.out.println("序列化再反序列化后 验证equals 和 hashcode："+o.equals(origin) +" , "+(o.hashCode()==origin.hashCode()));
    }
}
