package jaskey.antfinancial.interview.util;

import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Random;


public class BloomFilter {

    private final BitSet bs;
    final int [] hashSeeds;
    final int capacity;

    public BloomFilter(int slots, int hashFunctions) {
        bs = new BitSet(slots);
        Random r = new Random(System.currentTimeMillis());
        hashSeeds = new int[hashFunctions];
        for (int i=0; i<hashFunctions; ++i) {
            hashSeeds[i] = r.nextInt();
        }
        capacity = slots;
    }

    public void add(int value) {
        byte [] b = new byte[] {
            (byte)(value >>> 24),
            (byte)(value >>> 16),
            (byte)(value >>> 8),
            (byte)value};
        for (int i=0; i<hashSeeds.length; ++i) {
            int h = Hash.hash32(b, 4, hashSeeds[i]);
            bs.set(Math.abs(h)%capacity, true);
        }
    }

    public void clear() {
        bs.clear();
    }

    public boolean mightContain(int value) {
        byte [] b = new byte[] {
            (byte)(value >>> 24),
            (byte)(value >>> 16),
            (byte)(value >>> 8),
            (byte)value};
        for (int i=0; i<hashSeeds.length; ++i) {
            int h = Hash.hash32(b, 4, hashSeeds[i]);

            if (!bs.get(Math.abs(h) % capacity)) {
                return false;
            }
        }

        return true;
    }


    public static void main(String [] args) {
        BloomFilter bf = new BloomFilter(1000, 10);

        List<String> cases =Arrays.asList("case1","case2","case3","case4");
        List<String> nonExistCases =Arrays.asList("NONcase1","NONcase2","NONcase3","NONcase4");

        for (String cas: cases) {
            System.out.println("Query for  " + cas + " : "+bf.mightContain(cas.hashCode()));
        }

        System.out.println("Adding cases and check again");
        for (String cas: cases) {
            bf.add(cas.hashCode());
        }

        for (String cas: cases) {
            System.out.println("Query for  " + cas + " : "+bf.mightContain(cas.hashCode()));
        }

        System.out.println(" check the exist cases...");
        for (String cas: nonExistCases) {
            System.out.println("Query for  " + cas + " res :"+bf.mightContain(cas.hashCode()));
        }



    }
}