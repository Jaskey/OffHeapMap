package jaskey.antfinancial.interview.core;

import jaskey.antfinancial.interview.util.BloomFilter;
import jaskey.antfinancial.interview.proxy.EqualsProxy;
import jaskey.antfinancial.interview.proxy.ObjectOffHeapProxy;
import java.io.IOException;
import java.util.HashMap;

import java.util.Map;

/**
 * 堆外内存存储KV
 * 内部用hashmap保留映射骨架，结点本身指向真实数据
 *
 * 支持bloom fliter 快速过滤不命中场景
 */
public class OffHeapMap implements KVStore {
    private final Map<Object,ObjectOffHeapProxy>  skeleton;
    private final BloomFilter bloomFilter;//bloom filter
    private final boolean enableWeakRef;

    /**
     *
     * @param skeletonCapacity 使用的hashmap骨架的初始容量
     * @param bloomFilterSlots bloom filter数的slots数
     * @param bloomFilterHashFuncs bloom filter的哈希散列函数个数
     * @param enableWeakRef 是否启动weak reference引用
     */
    public OffHeapMap(int skeletonCapacity, int bloomFilterSlots , int bloomFilterHashFuncs , boolean enableWeakRef) {
        skeleton = new HashMap<>(skeletonCapacity,1.0f);
        this.enableWeakRef = enableWeakRef;
        if (bloomFilterHashFuncs >0 && bloomFilterSlots > 0) {
            bloomFilter = new BloomFilter(bloomFilterSlots, bloomFilterHashFuncs);
        }else {
            bloomFilter =null;
        }
    }

    public static void main(String[] args) {
        OffHeapMap kv = new OffHeapMap(100,1000,3,false);
        kv.put("1","hello1");
        kv.put("2","hello2");
        kv.put("3","hello3");
        kv.put("4", "hello4");
        kv.put("5", "hello5");
        kv.put("6", "hello6");

        System.out.println(kv.get("1"));
        System.out.println(kv.get("2"));
        System.out.println(kv.get("3"));
        System.out.println(kv.get("4"));
        System.out.println(kv.get("5"));
        System.out.println(kv.get("6"));
        System.out.println(kv.get("7"));
        System.out.println(kv.get("8"));
        System.out.println(kv.get("9"));
    }

    //用offheapproxy做key 存储
    @Override
    public void put(Object key, Object value) {
        try {
            //KV 代理，本身不存储对象，对象存堆外。
            ObjectOffHeapProxy offHeapKeyProxy = new ObjectOffHeapProxy(key,enableWeakRef);
            ObjectOffHeapProxy offHeapValueProxy = new ObjectOffHeapProxy(value,enableWeakRef);

            ObjectOffHeapProxy old = skeleton.put(offHeapKeyProxy, offHeapValueProxy);//存骨架架构
            if (bloomFilter!=null){
                bloomFilter.add(key.hashCode());//加bloom filter
            }

            if (old !=null) {//update 操作，立刻释放原value的堆外内存，避免在不触发GC的情况下堆外内存泄漏
                old.release();
            }

        }catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * get 的时候，会把key包装成EqualsProxy再get
     * @param key
     * @return
     */
    @Override
    public Object get(Object key) {
        if(bloomFilter!=null  && !bloomFilter.mightContain(key.hashCode())) {//bloom filter 没有，肯定不存在，不查询直接返回null
            return null;
        }

        ObjectOffHeapProxy proxy = skeleton.get(new EqualsProxy(key));
        if (proxy!=null){
            return proxy.findActual();
        }
        else {
            return null;
        }
    }

}
