package jaskey.antfinancial.interview.core;

/**
 * Created by jaskeylin on 2017/3/14.
 */
public interface KVStore {
    void put(Object key,Object value);
    Object get(Object key);
}
