package jaskey.antfinancial.interview.testcase;

import java.io.Serializable;

/**
 * Created by jaskeylin on 2017/3/15.
 * 测试的object，测试对象是否反序列化成功
 */
public class TestObject implements Serializable{
    private String val;

    public TestObject(String val) {
        this.val = val;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        TestObject object = (TestObject) o;

        if (val != null ? !val.equals(object.val) : object.val != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return val != null ? val.hashCode() : 0;
    }

    @Override public String toString() {
        return "TestObject{" + "val='" + val + '\'' + '}';
    }
}
