package jaskey.antfinancial.interview.testcase;

import jaskey.antfinancial.interview.core.KVStore;
import jaskey.antfinancial.interview.core.OffHeapMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * Created by jaskeylin on 2017/3/15.
 */
public class Benchmark {
    private static final int  HOW_MANY_WRITES = 100_0000;
    private static final int  HOW_MANY_NOTHIT = 100_0000;

    private static final int mapInitialCapacity = HOW_MANY_WRITES/10;

    public static void main(String[] args) {

        //带bloom filter
        OffHeapMap kv = new OffHeapMap(mapInitialCapacity,10_0000,3,true);
        goBenchmark(kv);

        //没有bloom filter
//        OffHeapMap kvNoBF = new OffHeapMap(mapInitialCapacity,0,0,true);
//        goBenchmark(kvNoBF);

//        对比普通hashmap性能
//        KVStore mapNormal = new KVStoreHashMap(new HashMap<>(mapInitialCapacity,1.0f));
//        goBenchmark(mapNormal);


    }

    private static void goBenchmark(KVStore kv) {
        long beginAll = System.currentTimeMillis();
        List<String> cases = new ArrayList<>(HOW_MANY_WRITES);//命中的场景
        for (int i=HOW_MANY_WRITES;i>0;i--){
            cases.add("HIT-CASE"+String.valueOf(i));
        }

        //插入操作
        {
            long beginWrite = System.currentTimeMillis();
            for (String cas : cases) {
                kv.put(cas,new TestObject("hello"+cas));
            }

            long cost = (System.currentTimeMillis() - beginWrite);
            double tps = (HOW_MANY_WRITES / (cost/1000.0f));
            System.out.println("put objects:  " + HOW_MANY_WRITES + " objects, cost : " +cost+  " ,tps: " + tps);
        }


        //更新操作
        {
            long beginUpdate = System.currentTimeMillis();
            for (String cas : cases) {
                kv.put(cas,new TestObject("update"+cas));
            }

            long cost = (System.currentTimeMillis() - beginUpdate);
            float tps = (HOW_MANY_WRITES / (cost/1000.0f));
            System.out.println("update objects:  " + HOW_MANY_WRITES + " objects, cost : " +cost+  " ,tps: " + tps);
        }

        //打散随机访问，读命中
        Collections.shuffle(cases);
        {
            long beginRead = System.currentTimeMillis();
            for (String cas : cases) {
                TestObject o = (TestObject) kv.get(cas);
                if (o == null) {
                    throw new RuntimeException("BUG HERE, can not find after put");
                }
            }

            long cost = (System.currentTimeMillis() - beginRead);
            float tps = (HOW_MANY_WRITES / cost) * 1000.0f;
            System.out.println("random access " + HOW_MANY_WRITES + " objects, hit! cost : " + cost + " ,tps: " + tps);

        }

        //读不命中
        List<String> notHitCases = new ArrayList<>(HOW_MANY_NOTHIT);//不命中的场景
        for (int i=HOW_MANY_NOTHIT;i>0;i--){
            notHitCases.add("NOT-HIT"+String.valueOf(i));
        }
        {
            long beginReadNoHit = System.currentTimeMillis();
            for (String cas : notHitCases) {
                Object o = kv.get(cas);
                if (o != null) {
                    throw new RuntimeException("BUG HERE, should not exist!");
                }
            }


            long cost = (System.currentTimeMillis() - beginReadNoHit);
            float tps = (HOW_MANY_NOTHIT / (cost/1000.0f));
            System.out.println("random access " + HOW_MANY_NOTHIT + " , no hit, cost : " + cost + ", tps: " + tps);

        }


        System.out.println("-------------total cost :"+(System.currentTimeMillis()-beginAll)+"----------------");
    }


}
