package jaskey.antfinancial.interview.testcase;

import jaskey.antfinancial.interview.core.KVStore;
import java.util.HashMap;

/**
 * Created by jaskeylin on 2017/3/15.
 */
public class KVStoreHashMap implements KVStore {

    private final HashMap map;

    public KVStoreHashMap(HashMap map) {
        this.map = map;
    }

    @Override
    public void put(Object key, Object value) {
        map.put(key,value);
    }

    @Override
    public Object get(Object key) {
        return map.get(key);
    }
}
