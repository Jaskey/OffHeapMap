package jaskey.antfinancial.interview.proxy;

import jaskey.antfinancial.interview.util.SerialUtil;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

/**
 * k/v,对象的的代理，实际存储在堆外
 */
public class ObjectOffHeapProxy {

    private final ByteBuffer buff;
    private WeakReference<Object> actualRef = null;//持有弱引用，若用户的对象还在堆，则使用，否则gc调了，这里也不会占用空间
    public ObjectOffHeapProxy(Object actual, boolean enableWeakRef) throws IOException {
        if(enableWeakRef){
            actualRef = new WeakReference<Object>(actual);
        }
        byte[] bytes = SerialUtil.serialize(actual);
        this.buff = ByteBuffer.allocateDirect(bytes.length);
        buff.put(bytes);
    }

    /**
     * 判断两个对象是否相等，和真是对象对比，真是对象相等则equals
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        return findActual().equals(o);
    }

    /**
     * 取hashcode的时候，取真实对象的hashcode
     * @return
     */
    @Override
    public int hashCode() {
        return findActual().hashCode();
    }

    public Object findActual() {
        if (actualRef!=null) {
            Object o = actualRef.get();
            if (o != null) {
                return o;
            }
        }

        try{
            buff.flip();
            byte[] target = new byte[buff.limit()];
            buff.get(target);
            return SerialUtil.deserialize(target);
//            return SerialUtil.deserialize(buff);
        }
        catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    public void release(){
        try {
            destroyDirectByteBuffer(this.buff);
        } catch (Exception ex){
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }


    private static void destroyDirectByteBuffer(ByteBuffer toBeDestroyed) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
        Method cleanerMethod = toBeDestroyed.getClass().getMethod("cleaner");
        cleanerMethod.setAccessible(true);
        Object cleaner = cleanerMethod.invoke(toBeDestroyed);
        Method cleanMethod = cleaner.getClass().getMethod("clean");
        cleanMethod.setAccessible(true);
        cleanMethod.invoke(cleaner);

    }
}
