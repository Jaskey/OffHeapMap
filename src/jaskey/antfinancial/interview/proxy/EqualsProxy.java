package jaskey.antfinancial.interview.proxy;

/**
 * Created by jaskeylin on 2017/3/15.
 * 做equals对称性的代理，双边任意一边equals都认为equals
 */
public class EqualsProxy {
    private final Object actual;

    public EqualsProxy(Object actual) {
        this.actual = actual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o != null) {
            return actual.equals(o) || o.equals(actual);
        }

        return false;


    }

    @Override
    public int hashCode() {
        return actual != null ? actual.hashCode() : 0;
    }
}
