/**
* Created by jaskeylin on 2017/3/14.
*/
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Date;

public class DirectByteBufferExample {

    public static void main (String [] args) throws Exception {

        ByteBuffer buffer = ByteBuffer.allocateDirect(10);
        byte[] data = {1,2};
        buffer.put(data);
        System.out.println("after write bytes " + buffer);
        read(buffer);

        buffer.clear();
        buffer.put("hello".getBytes());
        System.out.println("after write string " + buffer);
        read(buffer);
    }

    private static void read(ByteBuffer buffer){
        buffer.flip();
        byte[] target = new byte[buffer.limit()];
        buffer.get(target);

        System.out.println(new String(target));
    }
}